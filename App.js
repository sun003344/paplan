import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import Main from './src/components/Main';

import calendar from './src/components/calendar';
import Home from './src/components/Home';

export default createStackNavigator({
  Home: { screen: Home },
  Main: { screen: Main },
})

// import React, { Component } from 'react';
// import { View, Text } from 'react-native';
// import Main from './src/components/Main';
// import Home from './src/components/Home';



// class app extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//     };
//   }

//   render() {
//     return (
//       <Home/>
//     );
//   }
// }

// export default app;
