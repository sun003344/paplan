import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';


export default class Main extends Component {
  static navigationOptions = {
    header: null
}

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.headerBar}>
          <Image source={require('../image/logo.png')} style={styles.logo} />
          <Text style={styles.titleText}>PAPLAN</Text>
        </View>
        <View style={styles.center}>
          <ModalDropdown options={['1', '2', '3']} />
        </View>
        <View style={{ flex: 3, backgroundColor: 'steelblue' }}>

        </View>
      </View>

    );
  }
}

const styles = {
  headerBar: {
    height: 90,
    backgroundColor: '#0ce55d',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center'
  },
  logo: {
    width: 42,
    height: 42,
    marginRight: 10,
  },
  titleText: {
    fontSize: 30,
    fontWeight: "bold",
    fontStyle: "normal",
    textAlign: "left",
    color: "#0f0f0f"
  },
  center: {
    flex: 2,
    width: 80,
    height: 90,
    backgroundColor: 'white',

  }
}
