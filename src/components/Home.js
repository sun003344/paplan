import React, { Component } from 'react';
import { View, Text, ImageBackground, Image, Button } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import Main from './Main';

const App = createStackNavigator({
    Main: { screen: Main },
});

class Home extends Component {
    static navigationOptions = {
        header: null
    }

constructor(props) {
    super(props);
    this.state = {};
}

render() {
    const { navigate } = this.props.navigation;
    return (
        <View style={styles.headerStyle}>
            <ImageBackground source={require('../image/p.jpg')} style={styles.backgroundImage} >
                <Image style={styles.van} source={require('../image/van.png')}></Image>
                <View style={{ alignItems: "center" }}>
                    <Text style={styles.textHome}>PaPlan</Text>
                    <Button
                        title="มาวางแผนกันเถอะ"
                        onPress={() =>
                            this.props.navigation.navigate('Main')
                        }
                    />
                </View>
            </ImageBackground>
        </View>
    );
}
}

const styles = {
    textHome: {
        fontSize: 60,
        color: "black",
        fontWeight: 'bold'
    },
    headerStyle: {
        justifyContent: "center",
        alignItems: "center"
    },
    backgroundImage: {
        width: "100%",
        height: "100%",
        opacity: 0.8

    },
    van: {
        width: 125,
        height: 125,
        marginTop: 300,
        alignItems: 'center',
        marginLeft: 125
    }

}

export default Home;
